# Super NSS homework 6
##Authors
* Ivan Shalaev
* Shalaev Ivan
##Technologies Used
* Java Spring Boot
* JDBC
* PostgreSQL
* Java RMI
* Kubernetes
##Description
Lorem ipsum dolor _sit amet_, consectetur adipiscing elit. 
Nullam neque est, **consequat** a nunc quis, ornare aliquet enim. 
Cras sed turpis bibendum, mollis ipsum eu, ullamcorper felis. 
Suspendisse convallis tortor elit, sit amet **rhoncus** felis luctus id.
Curabitur et sem ut ante euismod elementum. 
Suspendisse et ipsum ut justo egestas.
##Setup/Instalation Requirements
1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
   1. Nullam neque est, consequat a nunc quis, ornare aliquet enim.
   2. Cras sed turpis bibendum, mollis ipsum eu, ullamcorper felis.
2. Suspendisse convallis tortor elit, sit amet rhoncus felis luctus id.
   1. Curabitur et sem ut ante euismod elementum.
3. Suspendisse et ipsum ut justo egestas.

> Donec euismod interdum sollicitudin. Maecenas maximus nibh est, eget laoreet nibh tincidunt ac.

#Known Bugs
| Bug Name  | Description          | Solution       |
|:---------|:---------------------|:---------------|
| Bug 1    | Description text 1.  | Solution 1 |
| Bug 2    | Description text 2.  | Solution 2 |
| Bug 3 | Desctiption text 3. | Solution 3| 
